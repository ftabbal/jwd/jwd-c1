package com.udacity.jwdnd.course1.cloudstorage.mapper;

import com.udacity.jwdnd.course1.cloudstorage.entity.File;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Mapper
public interface FileMapper {

    @Select("SELECT * FROM FILES WHERE userid=#{userid}")
    public List<File> getFileList(User user);

    @Select("SELECT * FROM FILES WHERE id=#{fileId} AND userid=#{userId}")
    public File getFileFromId(int fileId, int userId);

    @Select("SELECT filename FROM FILES WHERE filename=#{fileName} AND userid=#{userId}")
    public String getExistingFileName(String fileName, int userId);

    @Insert("INSERT INTO FILES (filename, contenttype, filesize, userid, filedata) VALUES " +
            "(#{filename}, #{contentType}, #{fileSize}, #{userId}, #{fileData})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    public void addFile(File file);

    @Delete("DELETE FROM FILES WHERE id=#{fileId}")
    public void deleteFile(int fileId);

}
