package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.entity.Credential;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.services.CredentialService;
import com.udacity.jwdnd.course1.cloudstorage.services.EncryptionService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.SecureRandom;
import java.util.Base64;

@Controller
@RequestMapping("/credentials")
public class CredentialController {

    private UserService userService;
    private CredentialService credentialService;
    private EncryptionService encryptionService;

    public CredentialController(UserService userService, CredentialService credentialService, EncryptionService encryptionService) {
        this.userService = userService;
        this.credentialService = credentialService;
        this.encryptionService = encryptionService;
    }

    @GetMapping
    public String credentialsView(Authentication authentication, Model model) {
        try {
            User user = userService.getUser(authentication.getName());
            if (user == null) {
                return "redirect:/login";
            }
            model.addAttribute("credential", new Credential());
            model.addAttribute("credentialList", credentialService.getCredentials(user));
            return "credentials";
        } catch (NullPointerException e) {
            return "redirect:/login";
        }
    }

    @PostMapping
    public String postCredential(Authentication authentication, Credential credential) {
        User user = userService.getUser(authentication.getName());
        if (user == null) {
            return "redirect:/login";
        }
        credential.setUserId(user.getUserid());
        if (credential.getId() == 0) {
            credentialService.addCredential(credential);
            return "redirect:/credentials?save=success";
        } else {
            credentialService.updateCredential(credential, user.getUserid());
            return "redirect:/credentials?save=edit-success";
        }
    }

    @GetMapping("{credentialId}")
    @ResponseBody
    public ResponseEntity<Credential> getCredential(@PathVariable(name = "credentialId") int credentialId, Authentication authentication) {
        try {
            User user = userService.getUser(authentication.getName());
            Credential credential = credentialService.getCredential(credentialId, user.getUserid());
            if (credential.getUrl() == "") {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(credential, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("delete/{credentialId}")
    public String deleteCredential(@PathVariable(name = "credentialId") String credentialId) {
        int id = Integer.parseInt(credentialId);
        credentialService.deleteCredential(id);
        return "redirect:/credentials?delete=success";
    }

    @ModelAttribute("encryptionService")
    public EncryptionService getEncryptionServiceDto() {
        return new EncryptionService();
    }

}
