package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.entity.Note;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.mapper.NoteMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteService {
    private NoteMapper noteMapper;

    public NoteService(NoteMapper noteMapper) {
        this.noteMapper = noteMapper;
    }

    public List<Note> getNotes(User user) {
        return noteMapper.getNotes(user);
    }

    public void addNote(Note note) {
        noteMapper.insert(note);
    }

    public void deleteNote(int noteId) {
        noteMapper.delete(noteId);
    }

    public Note getNote(int noteId) {
        Note note = noteMapper.getNoteById(noteId);
        return note;
    }

    public void updateNote(Note note) {
        noteMapper.updateNote(note);
    }

}
