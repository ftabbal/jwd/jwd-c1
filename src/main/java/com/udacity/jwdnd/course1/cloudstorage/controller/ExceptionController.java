package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.services.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class ExceptionController {
    // Class to handle files that are too big
    // Adapted from https://knowledge.udacity.com/questions/383238

    @Autowired
    private FileService fileService;

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public String handleException(MaxUploadSizeExceededException exception, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("upload", "error-big-file");
        return "redirect:/files";
    }
}
