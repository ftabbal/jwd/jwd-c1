package com.udacity.jwdnd.course1.cloudstorage.entity;

import java.text.CharacterIterator;
import java.text.DecimalFormat;
import java.text.StringCharacterIterator;

public class File {
    private int id;
    private String filename;
    private String contentType;
    private long fileSize;
    private int userId;
    private byte[] fileData;

    public File(int id, String filename, String contentType, long fileSize, int userId, byte[] data) {
        this.id = id;
        this.filename = filename;
        this.contentType = contentType;
        this.fileSize = fileSize;
        this.userId = userId;
        this.fileData = data;
    }

    public File() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public long getFileSize() {
        return fileSize;
    }

    public String getFileSizeAsString() {
        // from https://programming.guide/java/formatting-byte-size-to-human-readable-format.html
        if (-1000 < fileSize && fileSize < 1000) {
            return fileSize + " B";
        }
        CharacterIterator ci = new StringCharacterIterator("kMGTPE");
        while (fileSize <= -999_950 || fileSize >= 999_950) {
            fileSize /= 1000;
            ci.next();
        }
        return String.format("%.1f %cB", fileSize / 1000.0, ci.current());
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public byte[] getFileData() {
        return fileData;
    }

    public void setFileData(byte[] fileData) {
        this.fileData = fileData;
    }
}
