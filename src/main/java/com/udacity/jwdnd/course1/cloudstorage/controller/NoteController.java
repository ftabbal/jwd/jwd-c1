package com.udacity.jwdnd.course1.cloudstorage.controller;

import com.udacity.jwdnd.course1.cloudstorage.entity.Note;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.services.NoteService;
import com.udacity.jwdnd.course1.cloudstorage.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/notes")
public class NoteController {

    private UserService userService;
    private NoteService noteService;

    public NoteController(UserService userService, NoteService noteService) {
        this.userService = userService;
        this.noteService = noteService;
    }

    @GetMapping
    public String notesView(Authentication authentication, Model model) {
        try {
            User user = userService.getUser(authentication.getName());
            if (user == null) {
                return "redirect:/login";
            }
            model.addAttribute("note", new Note());
            model.addAttribute("noteList", noteService.getNotes(user));
            return "notes";
        } catch (NullPointerException e) {
            return "redirect:/login";
        }
    }

    @PostMapping
    public String postNote(Authentication authentication, Note note, Model model) {
        User user = userService.getUser(authentication.getName());
        if (user == null) {
            return "redirect:/login";
        }
        note.setUserId(user.getUserid());
        if (note.getId() > 0) {
            noteService.updateNote(note);
            return "redirect:/notes?save=edit-success";
        }
        else {
            noteService.addNote(note);
            return "redirect:/notes?save=success";
        }
    }

    @PostMapping("delete/{noteId}")
    public String deleteNote(Authentication authentication, @PathVariable(name = "noteId") String noteId) {
        User user = userService.getUser(authentication.getName());
        int id = Integer.parseInt(noteId);
        noteService.deleteNote(id);
        return "redirect:/notes?delete=success";
    }
}
