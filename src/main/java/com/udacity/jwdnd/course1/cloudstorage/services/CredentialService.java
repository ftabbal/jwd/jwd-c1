package com.udacity.jwdnd.course1.cloudstorage.services;

import com.udacity.jwdnd.course1.cloudstorage.entity.Credential;
import com.udacity.jwdnd.course1.cloudstorage.entity.User;
import com.udacity.jwdnd.course1.cloudstorage.mapper.CredentialMapper;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Base64;
import java.util.List;

@Service
public class CredentialService {

    private CredentialMapper credentialMapper;
    private EncryptionService encryptionService;


    public CredentialService(CredentialMapper credentialMapper, EncryptionService encryptionService) {
        this.credentialMapper = credentialMapper;
        this.encryptionService = encryptionService;
    }

    public List<Credential> getCredentials(User user) {
        return credentialMapper.getCredentials(user.getUserid());
    }

    public void addCredential(Credential credential) {
        SecureRandom random = new SecureRandom();
        byte[] key = new byte[16];
        random.nextBytes(key);
        String encodedKey = Base64.getEncoder().encodeToString(key);
        credential.setPassword(encryptionService.encryptValue(credential.getPassword(), encodedKey));
        credential.setKey(encodedKey);
        credentialMapper.addCredential(credential);
    }

    public Credential getCredential(int id, int userId) {
        Credential credential =  credentialMapper.getCredentialById(id, userId);
        if (credential == null) {
            return null;
        }
        String encodedKey = credential.getKey();
        String decryptedPassword = encryptionService.decryptValue(credential.getPassword(), encodedKey);
        credential.setPassword(decryptedPassword);
        return credential;
    }

    public void updateCredential(Credential credential, int userId) {
        Credential dbCredential = credentialMapper.getCredentialById(credential.getId(), userId);
        credential.setPassword(encryptionService.encryptValue(credential.getPassword(), dbCredential.getKey()));
        credentialMapper.updateCredential(credential);

    }

    public void deleteCredential(int credentialId) {
        credentialMapper.deleteCredential(credentialId);
    }
}
