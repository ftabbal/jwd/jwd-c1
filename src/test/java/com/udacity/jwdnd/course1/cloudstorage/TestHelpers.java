package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.page.HomePage;
import com.udacity.jwdnd.course1.cloudstorage.page.LoginPage;
import com.udacity.jwdnd.course1.cloudstorage.page.SignupPage;
import org.openqa.selenium.WebDriver;

public class TestHelpers {

    public static void signup(WebDriver driver, int port, String username, String password) {
        SignupPage signupPage = new SignupPage(driver);
        driver.get("http://localhost:" + port + "/signup");
        signupPage.register(username, "placeholder", username, password);
    }

    public static void login(WebDriver driver, int port, String username, String password) {
        LoginPage loginPage = new LoginPage(driver);
        driver.get("http://localhost:" + port + "/login");
        loginPage.login(username, password);

        if (loginPage.getErrorMessage() != "") {
            signup(driver, port, username, password);
            login(driver, port, username, password);
        }
    }
}
