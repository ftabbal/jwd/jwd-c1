package com.udacity.jwdnd.course1.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignupPage {

    @FindBy(id = "inputFirstName")
    WebElement firstNameField;

    @FindBy(id = "inputLastName")
    WebElement lastNameField;

    @FindBy(id = "inputUsername")
    WebElement usernameField;

    @FindBy(id = "inputPassword")
    WebElement passwordField;

    @FindBy(id = "submit-button")
    WebElement signupButton;

    @FindBy(id = "success-msg")
    WebElement successMessage;

    @FindBy(id = "error-msg")
    WebElement errorMessage;

    public SignupPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public void register(String firstName, String lastName, String username, String password) {
        firstNameField.sendKeys(firstName);
        lastNameField.sendKeys(lastName);
        usernameField.sendKeys(username);
        passwordField.sendKeys(password);
        signupButton.click();
    }

    public boolean signupSuccessful() {
        return successMessage.isDisplayed();
    }

    public boolean signupFailed() {
        return errorMessage.isDisplayed();
    }
}
