package com.udacity.jwdnd.course1.cloudstorage.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class NotePage extends AbstractPage {

    // Navigation Bar elements
    @FindBy(id = "nav-notes-tab")
    WebElement noteTab;

    // Note elements
    @FindBy(className = "note")
    List<WebElement> notes;

    @FindBy(id = "add-note-button")
    WebElement addNoteButton;

    @FindBy(id = "note-title")
    WebElement noteTitleField;

    @FindBy(id = "note-description")
    WebElement noteDescriptionField;

    @FindBy(id = "save-button")
    WebElement noteSubmitButton;

    @FindBy(className = "note-edit-button")
    List<WebElement> editButtonList;

    @FindBy(id = "delete-note-button")
    List<WebElement> deleteButtonList;

    @FindBy(className = "note-title")
    List<WebElement> noteTitleList;

    @FindBy(className = "note-description")
    List<WebElement> noteDescriptionList;

    WebDriver driver;

    public NotePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public void addNote(String noteTitle, String noteDescription) {
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        noteTab = wait.until(webDriver -> webDriver.findElement(By.id("nav-notes-tab")));
        noteTab.click();
        addNoteButton.click();

        noteTitleField = wait.until(webDriver -> webDriver.findElement(By.id("note-title")));

        noteTitleField.sendKeys(noteTitle);
        noteDescriptionField.sendKeys(noteDescription);
        noteSubmitButton.click();
    }

    public void updateNote(String newTitle, String newDescription, int index) {
        WebDriverWait wait = new WebDriverWait(driver, 1000);
        noteTab = wait.until(webDriver -> webDriver.findElement(By.id("nav-notes-tab")));
        noteTab.click();
        WebElement editButton = editButtonList.get(index);
        editButton.click();

        noteTitleField = wait.until(webDriver -> webDriver.findElement(By.id("note-title")));
        noteTitleField.clear();
        noteTitleField.sendKeys(newTitle);

        noteDescriptionField.clear();
        noteDescriptionField.sendKeys(newDescription);

        noteSubmitButton.click();
    }

    public void deleteNote(int index) {
        WebElement deleteButton = deleteButtonList.get(index);
        deleteButton.click();
    }

    public String getNoteTitle(int index) {
        WebElement noteTitle = noteTitleList.get(index);
        return noteTitle.getText();
    }

    public String getNoteDescription(int index) {
        WebElement noteDescription = noteDescriptionList.get(index);
        return noteDescription.getText();
    }

    public int getNumberOfNotes() {
        return notes.size();
    }
}
