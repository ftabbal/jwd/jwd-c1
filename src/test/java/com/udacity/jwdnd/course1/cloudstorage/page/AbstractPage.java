package com.udacity.jwdnd.course1.cloudstorage.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

// Base class to have the logout button for each mock pahe
public class AbstractPage {
    WebDriver driver;

    @FindBy(id = "logoutButton")
    WebElement logoutButton;

    public void logout() {
        logoutButton.click();
    }
}
