package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.page.CredentialPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CredentialTests {
    @LocalServerPort
    private int port;

    private WebDriver driver;
    private CredentialPage credentialPage;

    int initialNumberOfCredentials;

    @BeforeAll
    // I don't have Chrome on my computer, using Firefox instead
    static void beforeAll() {
        WebDriverManager.firefoxdriver().setup();
    }

    @BeforeEach
    public void beforeEach() {
        this.driver = new FirefoxDriver();
        TestHelpers.login(driver, port, "fred", "1234");
        credentialPage = new CredentialPage(driver);
        driver.get("http://localhost:" + port + "/credentials");
        initialNumberOfCredentials = credentialPage.getNumberOfCredentials();
    }

    @AfterEach
    public void afterEach() {
        credentialPage.logout();
        if (this.driver != null) {
            driver.quit();
        }
    }

    @Test
    void testCreateOneCredentials() {
        int addedEntryIndex = initialNumberOfCredentials;
        credentialPage.addCredentials("this", "fred", "1234");
        Assertions.assertEquals(initialNumberOfCredentials + 1, credentialPage.getNumberOfCredentials());

        Assertions.assertEquals("this", credentialPage.getTableCredentialUrl(addedEntryIndex));
        Assertions.assertEquals("fred", credentialPage.getTableCredentialUsername(addedEntryIndex));

        // Verify that the password does not display in clear text
        Assertions.assertNotEquals("1234", credentialPage.getTableCredentialPassword(addedEntryIndex));
    }

    @Test void testCreateAndUpdateCredentials() {
        int addedEntryIndex = initialNumberOfCredentials;
        credentialPage.addCredentials("google", "me", "hello!");
        Assertions.assertEquals("google", credentialPage.getTableCredentialUrl(addedEntryIndex));
        Assertions.assertEquals("me", credentialPage.getTableCredentialUsername(addedEntryIndex));
        Assertions.assertNotEquals("1234", credentialPage.getTableCredentialPassword(addedEntryIndex));

        String originalPassword = credentialPage.getDecryptedPassword(addedEntryIndex);
        String originalEncryptedPassword = credentialPage.getTableCredentialPassword(addedEntryIndex);

        Assertions.assertEquals("hello!", originalPassword);

        credentialPage.updateCredentials("google", "me", "5678", addedEntryIndex);
        Assertions.assertNotEquals(originalEncryptedPassword, credentialPage.getTableCredentialPassword(addedEntryIndex));

        String newPassword = credentialPage.getDecryptedPassword(addedEntryIndex);
        Assertions.assertNotEquals(originalPassword, newPassword);
        Assertions.assertEquals("5678", newPassword);
    }

    @Test
    void testCreateAndDeleteCredential() {
        int addedEntryIndex = initialNumberOfCredentials;
        credentialPage.addCredentials("to delete", "WillDelete", "1234");
        credentialPage.deleteCredentials(addedEntryIndex);
        Assertions.assertEquals(initialNumberOfCredentials, credentialPage.getNumberOfCredentials());
    }
}
