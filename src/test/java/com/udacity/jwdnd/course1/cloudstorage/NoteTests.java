package com.udacity.jwdnd.course1.cloudstorage;

import com.udacity.jwdnd.course1.cloudstorage.page.NotePage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NoteTests {
    @LocalServerPort
    private int port;

    private WebDriver driver;
    private NotePage notePage;

    private int initialNumberOfNotes;

    @BeforeAll
    // I don't have Chrome on my computer, using Firefox instead
    static void beforeAll() {
        WebDriverManager.firefoxdriver().setup();
    }

    @BeforeEach
    public void beforeEach() {
        this.driver = new FirefoxDriver();
        TestHelpers.login(driver, port, "fred", "1234");
        notePage = new NotePage(driver);
        driver.get("http://localhost:" + this.port + "/notes");
        initialNumberOfNotes = notePage.getNumberOfNotes();
    }

    @AfterEach
    public void afterEach() {
        notePage.logout();
        if (this.driver != null) {
            driver.quit();
        }
    }

    @Test
    public void testCreateOneNote() {
        int addEntryIndex = initialNumberOfNotes == 0 ? 0 : initialNumberOfNotes;
        notePage.addNote("Create One Note", "Create One Note Description");
        Assertions.assertEquals(initialNumberOfNotes + 1, notePage.getNumberOfNotes());
        Assertions.assertEquals("Create One Note", notePage.getNoteTitle(addEntryIndex));
        Assertions.assertEquals("Create One Note Description", notePage.getNoteDescription(addEntryIndex));
    }

    @Test
    public void testCreateTwoNotes() {
        notePage.addNote("Test Title", "Test Description");
        notePage.addNote("Test Title 2", "Test Description 2");
        Assertions.assertEquals(initialNumberOfNotes + 2, notePage.getNumberOfNotes());
    }

    @Test
    public void testCreateAndUpdateNote() {
        // Array index starts from zero.
        // If we add an entry, the initial number of entries becomes the index of the appended entry.
        // Assigning to a variable for readability
        int newEntryIndex = initialNumberOfNotes;
        notePage.addNote("Update note title", "Update note description");
        initialNumberOfNotes += 1;

        notePage.updateNote("Rawr!", "ゴジラだ！", newEntryIndex);
        Assertions.assertEquals("Rawr!", notePage.getNoteTitle(newEntryIndex));
        Assertions.assertEquals("ゴジラだ！", notePage.getNoteDescription(newEntryIndex));

        // Make sure that the note was not added
        Assertions.assertEquals(initialNumberOfNotes, notePage.getNumberOfNotes());
    }

    @Test
    public void testCreateUpdateDeleteNote() {
        int entryIndex = initialNumberOfNotes;
        notePage.addNote("Create Update Delete", "Create Update Delete Description");
        Assertions.assertEquals(initialNumberOfNotes + 1, notePage.getNumberOfNotes());
        //Create
        Assertions.assertEquals("Create Update Delete", notePage.getNoteTitle(entryIndex));
        Assertions.assertEquals("Create Update Delete Description", notePage.getNoteDescription(entryIndex));
        Assertions.assertEquals(initialNumberOfNotes + 1, notePage.getNumberOfNotes());
        //Update
        notePage.updateNote("Rawr!", "モスラだ！", entryIndex);
        Assertions.assertEquals("Rawr!", notePage.getNoteTitle(entryIndex));
        Assertions.assertEquals("モスラだ！", notePage.getNoteDescription(entryIndex));
        //Delete
        notePage.deleteNote(entryIndex);
        Assertions.assertEquals(initialNumberOfNotes, notePage.getNumberOfNotes());
    }

}
